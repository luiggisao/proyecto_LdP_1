require './grafo.rb'

class ExpresionRegular
	@@epsilon = "epsilon"
	def initialize(expresionRegular)
		@expresionRegular = expresionRegular.gsub 'OR_EXPRESSION', '|' 
		@tamanoGrafo = 0
		@pesos = Array.new()
		setPesos()
		#Se necesitó cambiar | por OR_EXPRESSION por errores producidos en el POST request
	end

	attr_accessor :expresionRegular, :pesos

	def separarOrExterno(expresion)
		arr = Array.new
		bandera = 0
		builder = ""
		(0..(expresion.length-1)).each do |n|
			if(bandera != 0) 
				builder << (expresion[n]).to_s
				if((expresion[n]).to_s == "(")
					bandera += 1
				elsif ((expresion[n]).to_s == ")")
					bandera -= 1
				end
			else 
				if((expresion[n]).to_s == "|")
					arr << builder
					builder = ""
				elsif ((expresion[n]).to_s == "(")
					builder << (expresion[n]).to_s
					bandera += 1
				else 
					builder << (expresion[n]).to_s
				end 
			end
		end
		arr << builder
		arr
	end

	def setPesos()
		(0..@expresionRegular.length-1).each do |n|
			if(@expresionRegular[n] != "(" && @expresionRegular[n] != ")" && @expresionRegular[n] != "*" && @expresionRegular[n] != "+" && @expresionRegular[n] != "|")
				@pesos << @expresionRegular[n]
			end
		end 
		@pesos = @pesos.uniq
	end

	def separarEnConcat(expresion)
		arr = Array.new
		bandera = 0 #Se usa esta bandera para separar la expresión
		builder = ""
		(0..(expresion.length-1)).each do |n|
			if(bandera != 0)  #Si es diferente de 0, esto indica que hay un paréntesis abierto
				builder << (expresion[n]).to_s
				if((expresion[n]).to_s == "(")
					bandera += 1 # Si se abre paréntesis se le suma 1 a la bandera
				elsif ((expresion[n]).to_s == ")")
					bandera -= 1 # Si se cierra paréntesis se le resta 1 a la bandera
					if(bandera == 0)
						arr << builder
						builder = ""
					end 
				end
			else 
				if((expresion[n]).to_s == "*")
					arr[arr.length-1] = arr[arr.length-1]+"*"
					#arr << builder
					#builder = ""
				elsif((expresion[n]).to_s == "+")
					arr[arr.length-1] = arr[arr.length-1]+"+"

				elsif ((expresion[n]).to_s == "(")
					builder << (expresion[n]).to_s
					bandera += 1

				elsif ((expresion[n]).to_s == "|")
					builder << arr.pop 
					builder << "|"

				else 
					builder << (expresion[n]).to_s
					arr << builder
					builder = ""
				end 
			end
		end
		arr
	end

	def stripParentesis(expresion)
		if(expresion[0] == "(" && expresion[expresion.length-1] == ")")
			if(cantidadDeParentesisIgualesSeparados(expresion)<=1)
				builder = ""
				(1..(expresion.length-2)).each do |n| 
					builder << (expresion[n]).to_s
				end
				return builder
			end
		end 
		expresion
	end 

	def cantidadDeParentesisIgualesSeparados(expresion)
		parentesis = 0
		cant = 0
		(0..expresion.length-1).each do |n|
			if(expresion[n] == "(")
				parentesis += 1
			elsif(expresion[n] == ")")
				parentesis -=1
				if (parentesis == 0)
					cant += 1
				end 
			end  
		end
		cant
	end

	def faltaProcesar(grafo)
		(grafo.edges).each do |edge|
			if((edge.peso).length>1)
				return true
			end 
		end
		return false
	end

	def procesarOr(grafo)
		(grafo.edges).each do |edge|
			if((edge.peso).length>1)
				pesoSeparadoOr = separarOrExterno(edge.peso)
				if(pesoSeparadoOr.length > 1)
					grafo.removeEdge(edge.origen.etiqueta, edge.destino.etiqueta, edge.peso)

					(0..pesoSeparadoOr.length-1).each do |n|
						grafo.addEdge(edge.origen.etiqueta, edge.destino.etiqueta, stripParentesis(pesoSeparadoOr[n]))
					end
				end
			end 
		end 
	end 

	def procesarConcat(grafo)
		(grafo.edges).each do |edge|
			if((edge.peso).length > 1)
				pesoSeparadoConcat = separarEnConcat(edge.peso)
				if(pesoSeparadoConcat.length > 1)
					grafo.removeEdge(edge.origen.etiqueta, edge.destino.etiqueta, edge.peso)

					grafo.addVertex((@tamanoGrafo+1).to_s, false)
					grafo.addEdge(edge.origen.etiqueta,(@tamanoGrafo+1).to_s, stripParentesis(pesoSeparadoConcat[0]))
					@tamanoGrafo += 1

					(1..pesoSeparadoConcat.length-1).each do |n|
						if(n == pesoSeparadoConcat.length-1) 
							grafo.addEdge((@tamanoGrafo).to_s,edge.destino.etiqueta, stripParentesis(pesoSeparadoConcat[n]))
						else 
							@tamanoGrafo += 1
							grafo.addVertex((@tamanoGrafo).to_s, false)
							grafo.addEdge((@tamanoGrafo-1).to_s,(@tamanoGrafo).to_s, stripParentesis(pesoSeparadoConcat[n]))
						end 
					end 
				end 
			end 
		end 
	end

	def procesarKleene(grafo)
		(grafo.edges).each do |edge| 
			if((separarEnConcat(edge.peso)).length == 1)
				if(edge.peso[-1] == "*") #TODO revisar desde qué versión de Ruby se puede hacer esto
					grafo.removeEdge(edge.origen.etiqueta, edge.destino.etiqueta, edge.peso)
					pesoSinAsterisco = stripParentesis(edge.peso[0...-1])
					
					@tamanoGrafo += 1
					grafo.addVertex((@tamanoGrafo).to_s, false)
					grafo.addEdge(edge.origen.etiqueta, (@tamanoGrafo).to_s, @@epsilon)
					grafo.addEdge(edge.origen.etiqueta, edge.destino.etiqueta, @@epsilon)

					@tamanoGrafo += 1
					grafo.addVertex((@tamanoGrafo).to_s, false)
					grafo.addEdge((@tamanoGrafo-1).to_s, (@tamanoGrafo).to_s, pesoSinAsterisco)
					grafo.addEdge((@tamanoGrafo).to_s, edge.destino.etiqueta, @@epsilon)
					grafo.addEdge(edge.destino.etiqueta, (@tamanoGrafo-1).to_s, @@epsilon)

				elsif (edge.peso[-1] == "+")
					grafo.removeEdge(edge.origen.etiqueta, edge.destino.etiqueta, edge.peso)
					pesoSinAsterisco = stripParentesis(edge.peso[0...-1])

					@tamanoGrafo += 1
					grafo.addVertex((@tamanoGrafo).to_s, false)
					grafo.addEdge(edge.origen.etiqueta, (@tamanoGrafo).to_s, pesoSinAsterisco)
					
					@tamanoGrafo += 1
					grafo.addVertex((@tamanoGrafo).to_s, false)
					grafo.addEdge((@tamanoGrafo-1).to_s, (@tamanoGrafo).to_s, @@epsilon)
					grafo.addEdge((@tamanoGrafo-1).to_s, edge.destino.etiqueta, @@epsilon)

					@tamanoGrafo += 1
					grafo.addVertex((@tamanoGrafo).to_s, false)
					grafo.addEdge((@tamanoGrafo-1).to_s, (@tamanoGrafo).to_s, pesoSinAsterisco)
					grafo.addEdge((@tamanoGrafo).to_s, edge.destino.etiqueta, @@epsilon)
					grafo.addEdge(edge.destino.etiqueta, (@tamanoGrafo-1).to_s, @@epsilon)
				end 
			end 
		end
	end 

	def parseExpresionRegularAGrafo()
		grafo = Grafo.new()
		grafo.addVertex("1", true)
		grafo.addVertex("2", true)
		@tamanoGrafo = 2

		exprSeparadaOr = separarOrExterno(@expresionRegular)
		(exprSeparadaOr).each do |expr|
			grafo.addEdge("1", "2", stripParentesis(expr))
		end 

		procesarConcat(grafo)

		procesarKleene(grafo)

		while (faltaProcesar(grafo)) do
			puts "falta procesar grafo: "
			puts grafo.pesos
			procesarOr(grafo)
			procesarConcat(grafo)
			procesarKleene(grafo)
		end
		grafo
	end
	
	def grafoADot(grafo)
		if(grafo.vertices == 0)
			return nil
		end 
		builder = "digraph nfa {\n\t\"\" [shape=none]\n"
		(grafo.vertices).each do |vertice|
			builder << "\t\""+vertice.etiqueta.to_s+"\" [shape="
			if(vertice.aceptacion)
				builder << "doublecircle]\n"
			else 
				builder << "circle]\n"
			end
		end 
		builder << "\n\n\t\"\" -> \"1\"\n"
		(grafo.vertices).each do |vertice|
			(vertice.edges).each do |edge|
				if(!((edge.origen.etiqueta).to_s == (edge.destino.etiqueta).to_s && edge.peso == @@epsilon))
					builder << "\t\""+(edge.origen.etiqueta).to_s+"\" -> \""+(edge.destino.etiqueta).to_s+"\" [label=\""+(edge.peso).to_s+"\"]\n"
				end
			end
		end 
		builder << "}"
		builder
	end 
end

