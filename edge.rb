
class Edge
	attr_accessor :origen, :destino, :peso
	def initialize(origen, destino, peso)
		@origen = origen
		@destino = destino
		@peso = peso
	end

	def Yala 
		origen.edges.each do |edge|
			v0 = edge.origen
			vd = edge.destino
			peso = edge.peso
			if ((self.origen == v0) && (self.destino == vd) && (self.peso == peso))
				return true
			end
			
		end
		return false
	end

	def isEpsilon
		return peso == "epsilon"
	end

	def equals(edge)
		return (edge.destino == destino && edge.origen == origen && edge.peso == peso)
	end
end