require './grafo.rb'
EPSILON = "epsilon"
class Transition
	attr_accessor :grafo, :matriz, :pesos, :newGrafo
	def initialize(grafo)
		@grafo = grafo
		@newGrafo= Grafo.new()
		@pesos = []
		self.addPesos
	end

	def llenarEstados
		#newGrafo.pesos = pesos
		newGrafo.addVertex(grafo.vertices[0].etiqueta,true)
		vorigen = newGrafo.vertices[0]
		grafo.edges.each do |edge|
			vdestino = edge.destino
			buffer = ""
			aceptacion = self.llegada(vdestino, buffer)
			newGrafo.addVertex(buffer,aceptacion)
			if ((edge.origen.tieneEpsilonAnterior || edge.origen.equals(grafo.vertices[0]) || edge.destino.verticesEpsilon.size() > 2) && edge.destino.verticesEpsilon.size() > 1 )
				newVertice = newGrafo.vertices.last()
				newVertice.tieneEpsilonAnterior = true
			end
		end
		newGrafo.borrarpesoEpsilon()
		self.addEdges
	
	end

	def llegada(vertice, buffer)
		indice = 0
		aceptacion = false
		lista = vertice.verticesEpsilon
		lista.each do |v|
			if (indice == lista.size()-1)
				buffer << v.etiqueta
			else
				buffer << v.etiqueta+","
			end
			if (v.verticesEpsilon.size > 1 && !(v.equals(vertice)))
				lista2 = v.verticesEpsilon
				lista2.each do |v2|
					if (!(buffer.include?(v2.etiqueta)) ) 
						llegada(v2,buffer<<",")
					end
				end
			end		
			if (v.aceptacion)
				aceptacion = true	
			end			
			indice+=1
		end
		return aceptacion
	end

	def addEdges
		vertices = self.newGrafo.vertices
			vertices.each do |vertice|
				etiquetas = vertice.etiqueta.split(",")
				if (vertice.tieneEpsilonAnterior)
					etiquetas.each do |etiqueta|
						indice = grafo.searchVertex(etiqueta)
						vOrigen = grafo.vertices[indice]
						listaEdgesO = vOrigen.edges
							listaEdgesO.each do |edge|
								buffer = ""
								if (edge.peso != "epsilon")
									vdestino = edge.destino
									aceptacion = self.llegada(vdestino, buffer)
									newGrafo.addEdge(vertice.etiqueta,buffer,edge.peso)
								end	
							end
					end
				elsif (etiquetas.size() == 1)
					etiqueta = etiquetas[0]
					indice = grafo.searchVertex(etiqueta)
					vOrigen = grafo.vertices[indice]
					listaEdgesO = vOrigen.edges
						listaEdgesO.each do |edge|
							buffer = ""
							if (edge.peso != "epsilon")
								vdestino = edge.destino
								aceptacion = self.llegada(vdestino, buffer)
								newGrafo.addEdge(vertice.etiqueta,buffer,edge.peso)
							end	
						end
				end
					
			end

		
	end

	def tieneEpsilon (vertice)
		if (vertice.verticesEpsilon.size() > 1)
			vertice.tieneEpsilonAnterior = true
			return true
		end
		return false
	end

	def addPesos
		grafo.pesos.each do |peso|
			self.pesos.push(peso)
		end
	end	
end





