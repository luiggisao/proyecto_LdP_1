require "sinatra"
require "./ExpresionRegular.rb"
require "./transition.rb"

class LocalSinatra < Sinatra::Base
	get '/' do 
		send_file 'index.html'
		@textf = "aa"
	end

	post '/:automata' do
		re = ExpresionRegular.new(params[:automata])
		nfa = re.parseExpresionRegularAGrafo()
		nfa.letPesos
		transition = Transition.new(nfa)
		transition.llenarEstados
		re.grafoADot(nfa)+"SEPARATION"+transition.grafo.to_s+"SEPARATION"+transition.newGrafo.to_s+"SEPARATION"+transition.newGrafo.grafoADot
	end

end