var nfa = "";
var dfa = "";

function sendExpression() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var response = (xhttp.responseText).split("SEPARATION");
      nfa = response[0];
      console.log(nfa)
      var tablaSinEpsilon = response[1];
      console.log(tablaSinEpsilon);
      var tablaConEpsilon = response[2];
      console.log(tablaConEpsilon)
      dfa = response[3];
      console.log(dfa);
      window.renderDot(nfa, "nfa");
      createTable(tablaSinEpsilon, "tablaSinEpsilon");
      createTable(tablaConEpsilon, "tablaConEpsilon");
      window.renderDot(dfa, "dfa");
    }
  };
  var strToSend = (document.getElementById("regularExpression").value).replaceAll("|", "OR_EXPRESSION");
  xhttp.open("POST", strToSend, false);
  xhttp.send();
}

function createTable(tableString, tableName) {
  var filasTabla = tableString.split("|");
  var table = document.getElementById(tableName);
  table.className="flat-table";
  var tableBody = document.createElement("TBODY");

  table.appendChild(tableBody);

  for (var i=0; i<filasTabla.length-1; i++) { //-1 para compensar por la fila extra causada por "|" del final
    var tr = document.createElement("TR");
    tableBody.appendChild(tr);
    var columnasTabla = (filasTabla[i]).split(".");

    for(var j = 0; j < columnasTabla.length; j++) {
      if( i == 0) {
        var td = document.createElement("TH");
      } 
      else {
        var td = document.createElement("TD");
      }
      var text = document.createTextNode(columnasTabla[j]);
      td.appendChild(text);
      tr.appendChild(td);
    }
  }

  document.body.appendChild(table);
}

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.split(search).join(replacement);
};
