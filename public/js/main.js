
window.renderDot = function(fsm, divName) {
  var visualize = require('./visualize');
  const Viz = require('./viz.js');
  const { Module, render } = require('./full.render.js');

  let viz = new Viz({ Module, render });

  viz.renderSVGElement(fsm)
    .then(function(element) {
      //document.body.appendChild(element);
      document.getElementById(divName).appendChild(element);
    })
    .catch(error => {
      // Create a new Viz instance (@see Caveats page for more info)
      viz = new Viz({ workerURL });

      // Possibly display the error
      console.error(error);
    });
}
